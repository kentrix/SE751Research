# "Big Data" processing with MapReduce framework
![](mapreduce.jpg)<sup>1</sup>      

## Runnables
* The latest binaries for all implementations are found zipped on the [releases page](https://github.com/qhua948/SE751Research/releases)
* This includes inputs and instructions to run so you can reproduce our results

## K-means (main) implementation
* The source code for the k-means implementation is found under the [k-means directory](https://github.com/qhua948/SE751Research/tree/master/k-means/spark-scala-kmeans)
* This includes instructions to run
* The input files used in our benchmarks (namely 10MP.jpg) are found under [src/main/resources](https://github.com/qhua948/SE751Research/tree/master/k-means/spark-scala-kmeans/src/main/resources)

## reddit comment implementation
* The source code for the reddit comment implementations is found under the [reddit-comments directory](https://github.com/qhua948/SE751Research/tree/master/reddit-comments)
* This has been grouped by framework (couchDB, Hadoop, Spark, Cloud Haskell)
* The sequential Java version is found within the Hadoop source code or [here](https://github.com/qhua948/SE751Research/blob/master/reddit-comments/hadoop-reddit/src/main/java/nz/ac/auckland/mapreduce/NoFrameWorkMain.java)
* The input file used in our benchmarks was taken from [here](https://www.kaggle.com/reddit/reddit-comments-may-2015). We uncompressed it and took the first 20,000,000 lines (approx 11GB of JSON).

<sup>1</sup> Image credit: http://www.well-typed.com/blog/73/
