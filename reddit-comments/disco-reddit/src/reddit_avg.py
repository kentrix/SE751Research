from disco.core import Job, result_iterator
import json as simplejson
import os

def map(line, params):
    r = simplejson.loads(line).get('data').get('children')
    for index, word in enumerate(r):
        yield r[index].get('data').get('subreddit'), r[index].get('data').get('score')
	

def reduce(iter, params):
    cnt = 0
    avg = 0
    from disco.util import kvgroup
    for word, counts in kvgroup(sorted(iter)): 
	global cnt
	global avg 
	cnt += 1
	avg = avg + (cnt - avg)/cnt
        yield word, avg
	#yield word, iter

if __name__ == '__main__':
    #job = Job().run(input=["/home/hduser/disco/examples/util/tmp.json"],
    dir = os.getcwd() + "/Resources/tmp.json"
    job = Job().run(input=[dir],
                    map=map,
                    reduce=reduce)
    for word, count in result_iterator(job.wait(show=True)):
        print word, count
